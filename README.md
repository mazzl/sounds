# README #

To build the program, use CMake on the top level of the source tree and the command make from the same directory.

### What is this repository for? ###

* Project for "Laboratorio di Ingegneria Informatica"
* version 0.3

### How do I get set up? ###

* Use CMake + make or run compile.sh (Mac only)
* You could also compile from XCode using the Sounds.xcodeproj file
* To test the program from the command line use the file input/smoke\_on\_the\_water.strings or run test.sh (Mac only)

### Who do I talk to? ###

* Repo owner or admin