/** Project for "Laboratorio di Ingegneria Informatica"
 * @copyright Copyright © 2016 Luca Mazzon. All rights reserved.
 *
 * @file callback.c
 *
 * @brief This file contains the Callback funcion to be called by the Portaudio engine
 * @author Luca Mazzon
 * @since 0.3
 */

#include <stdio.h>
#include <math.h>
#include "portaudio.h"
#include "callback.h"

extern double f;
extern PaTestData data;

/** @brief The callback function
  * @details Writes a sinusoidal wave to the output buffer coherently to the previous phases whenever the Portaudio engine needs them. On some platforms this function could be called as an interrupt so it is forbidden to allocate memory dynamically or to do some other things that could mess up the system.
  *
  * @param input pointer to the input buffer (unused)
  * @param output pointer to the output buffer
  * @param framesPerBuffer dimension of each buffer
  * @param timeInfo info of the time (unused)
  * @param statusFlags flags for the status of the callback function (unused)
  * @param userData data passed by the user (the phases)
  *
  * @return 0 if there were no troubles, 1 otherwise
  *
  * since 0.1
  * @see PaStreamCallback in file portaudio.h
  */
int streamCallback(const void *input,
                          void *output,
                          unsigned long framesPerBuffer,
                          const PaStreamCallbackTimeInfo* timeInfo,
                          PaStreamCallbackFlags statusFlags,
                          void *userData)
{
    (void) input; //we don't use input buffer (preventing possible unused variable warning)
    
    //we don't use timeInfo and statusFlags
    
    PaTestData *data = (PaTestData *) userData; //cast userData to fit our structure
    
    float *out = (float *)output; //our output values are of tipe float
    float left_phi0 = data->left_phase;
    float right_phi0 = data->right_phase;
    
    //fill the buffer with a sine wave
    unsigned int i;
    for(i=0; i < framesPerBuffer; i++)
    {
        *out++ = sin(data->left_phase); //write sine of left phase on output buffer
        *out++ = sin(data->right_phase); //write sine of right phase on output buffer
        //update phases
        data->left_phase = 2*PI*(f-1)*(i+1)*Ts + left_phi0;
        if (data->left_phase >= 2*PI)
        {
            data->left_phase -= 2*PI; //to avoid loss of precision and overflow
        }
        data->right_phase = 2*PI*(f+1)*(i+1)*Ts + right_phi0;
        if (data->right_phase >= 2*PI)
        {
            data->right_phase -= 2*PI;
        }
        
    }
    return 0;
}