/** Project for "Laboratorio di Ingegneria Informatica"
 * @copyright Copyright © 2016 Luca Mazzon. All rights reserved.
 *
 * @file callback.h
 *
 * @brief This header contains the typedef of the struct containing the phases of the waveforms and the declaration of the callback funtion.
 * @author Luca Mazzon
 * @since 0.3
 */

#include "portaudio.h"

#ifndef callback_h
#define callback_h

#ifndef PI
#define PI 3.1415
#endif

#define SAMPLE_RATE (44100) //cd audio sampling frequency
#undef Ts //just in case...
#define Ts (1.0/SAMPLE_RATE)

/** @brief Keeps track of the current phases
 *  @details This struct is initialized to zero values in the main function and is used by the callback funtion to keep track of the last phases of the left and right sinusoidal waves.
 *
 */
typedef struct {
    float left_phase;
    float right_phase;
} PaTestData;

extern int streamCallback(const void *input,
                   void *output,
                   unsigned long framesPerBuffer,
                   const PaStreamCallbackTimeInfo* timeInfo,
                   PaStreamCallbackFlags statusFlags,
                          void *userData);

#endif /* callback_h */
