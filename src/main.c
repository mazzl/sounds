/** Project for "Laboratorio di Ingegneria Informatica"
  * @copyright Copyright © 2016 Luca Mazzon. All rights reserved.
  *
  * @file main.c
  *
  * @brief Generates and plays sinusoidal waveforms with the frequency of the notes specified by the user or plays notes read from a file with also the duration specified.
  * @author Luca Mazzon
  * @version 0.3
  * @since 0.1
  */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "portaudio.h"
#include "notes.h"
#include "callback.h"
#include "mainflows.h"

/** @brief The frequency of the note
  */
double f = 0.0;

/** @brief This struct contains the current phases of the waves
  * @see PaTestData
  */
PaTestData data;

/** @brief The main function of the program
  * @details Initialises the PortAudio engine and the wave phases; asks the user for the desired BPM; decides to call two function depending on the number of the arguments: no argument on the command line means playing from standard input, one argument means playing from file; frees resources used by the Portaudio engine at the end.
  * @param argc The number of arguments passed via the command line.
  * @param argv The file to read notes and durations from.
  * @return exit status
  * @see playFromFile
  * @see playFromStdIn
  */
int main(int argc, char *argv[])
{
    PaStream *stream;
    PaError error = paNoError;
    
    //initialize data
    data.left_phase = data.right_phase = 0.0;
    int bpm;
    printf("Insert rhythm in BPM: ");
    scanf("%d", &bpm);
    double beatDuration = (1.0/bpm)*60*1000; /* duration of one beat in milliseconds */
    
    //initialize PortAudioEngine
    error = Pa_Initialize();
    if(error != paNoError)
    {
        printf("PortAudio error: %s\n", Pa_GetErrorText(error));
        return EXIT_FAILURE;
    }
    
    if (argc == 2) {
        if (playFromFile(stream, argv[1], beatDuration) == EXIT_FAILURE) {
            return EXIT_FAILURE;
        }
    } else {
        if (playFromStdIn(stream, beatDuration) == EXIT_FAILURE) {
            return EXIT_FAILURE;
        }
    }
    
    error = Pa_Terminate();
    if(error != paNoError)
    {
        printf("PortAudio error: %s\n", Pa_GetErrorText(error));
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}
