/** Project for "Laboratorio di Ingegneria Informatica"
 * @copyright Copyright © 2016 Luca Mazzon. All rights reserved.
 *
 * @file mainflows.c
 *
 * @since 0.3
 * @brief This file contains the two functions called by the main whether it needs to play from standard input or to play from file.
 * @author Luca Mazzon
 * @since 0.3
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "portaudio.h"
#include "notes.h"
#include "callback.h"
#include "mainflows.h"

extern double f;
extern PaTestData data;

/** @brief Manages the reading from standard input
  *
  * @details playFromStdIn Asks the user to insert a note until he inserts Q to quit. Decides the frequency of the sinusoidal waveforms depending on the note. Open and set the streams to play the waveform with that frequency and for four beats. The duration of the notes depends on the BPM set in the main. Closes the stream after four beats.
  *
  * @param stream The stream initialized by the main.
  * @param beatDuration The duration of one beat
  * @return exit status
  * 
  * @since 0.3
  * @see notes.h
  */
int playFromStdIn(PaStream *stream, double beatDuration) {
    PaError error = paNoError;
    int exit = 0;
    while(!exit) {
        char note[3];
        printf("\nPrint note you want to listen (Q to quit): ");
        scanf("%2s", note);
        f=0.0;
        for(int i=0;i<sizeof(note);i++)
        {
            note[i]=toupper(note[i]);
        }
        
        switch(note[0])
        {
            case 'Q':
                exit = 1;
                break;
            case 'C':
                if ( note[1] == 'S') {
                    f = CSHARP4;
                    printf("Playing C sharp...");
                } else {
                    f = C4;
                    printf("Playing C...");
                }
                break;
            case 'D':
                if ( note[1] == 'F') {
                    f = DFLAT4;
                    printf("Playing F flat...");
                } else if (note [1] == 'S') {
                    f = DSHARP4;
                    printf("Playing D sharp...");
                } else {
                    f = D4;
                    printf("Playing D...");
                }
                break;
            case 'E':
                if ( note[1] == 'F') {
                    f = EFLAT4;
                    printf("Playing E flat...");
                } else {
                    f = E4;
                    printf("Playing E...");
                }
                break;
            case 'F':
                if ( note[1] == 'S') {
                    f = FSHARP4;
                    printf("Playing F sharp...");
                } else {
                    f = F4;
                    printf("Playing F...");
                }
                break;
            case 'G':
                if ( note[1] == 'F') {
                    f = GFLAT4;
                    printf("Playing G flat...");
                } else if (note [1] == 'S') {
                    f = GSHARP4;
                    printf("Playing G sharp...");
                } else {
                    f = G4;
                    printf("Playing G...");
                }
                break;
            case 'A':
                if ( note[1] == 'F') {
                    f = AFLAT4;
                    printf("Playing A flat...");
                } else if (note [1] == 'S') {
                    f = ASHARP4;
                    printf("Playing A sharp...");
                } else {
                    f = A4;
                    printf("Playing A...");
                }
                break;
            case 'B':
                if ( note[1] == 'F') {
                    f = BFLAT4;
                    printf("Playing B flat...");
                } else {
                    f = B4;
                    printf("Playing B...");
                }
                break;
            default:
                printf("Not a note.");
        }
        if ( f!= 0)
        {
            //open output stream (using default output device) with 0 input channels, 2 output channels (stereo) and 256 frames per buffer
            error = Pa_OpenDefaultStream(&stream, 0, 2, paFloat32, SAMPLE_RATE, 256, streamCallback, &data);
            if(error != paNoError)
            {
                printf("PortAudio error: %s\n", Pa_GetErrorText(error));
                return EXIT_FAILURE;
            }
            
            //start the stream
            error = Pa_StartStream(stream);
            if(error != paNoError)
            {
                printf("PortAudio error: %s\n", Pa_GetErrorText(error));
                return EXIT_FAILURE;
            }
            
            //sleep while stream is running (for 5 seconds)
            Pa_Sleep(4*beatDuration);
            
            //close stream after 5 seconds of streaming
            error = Pa_CloseStream(stream);
            if(error != paNoError)
            {
                printf("PortAudio error: %s\n", Pa_GetErrorText(error));
                return EXIT_FAILURE;
            }
        }
    }
    return EXIT_SUCCESS;
}

/** @brief Manages the reading from a file
  * @details Tryes to open a file with name "filename" passed to the main via the command line. Reads all the file playing one note at a time. Unlike playFromStdIn, it supports also pauses ('P'). The format of the strings in the file must be 'main note (C, D, E, F, G, A, B)' + 'pitch of the note (F for flat, S for sharp, '.' for none)' + 'whitespace (' ')' + 'number of beats ([0-9] or H for half a beat or '.' for one and a half beat)' + '\\n'.
 
  * @param stream the stream initialized by the main
  * @param filename the name of the file to be read
  * @param beatDuration the duration of one beat
  * @return exit status
 
  * @since 0.3
  * @see notes.h
  */
int playFromFile(PaStream *stream, char *filename, double beatDuration){
    PaError error = paNoError;
    printf("\nPlaying from file... \n");
    FILE *fp = fopen(filename, "r");
    if (fp == NULL) {
        perror("Error opening file");
        return EXIT_FAILURE;
    }
    char note[6]; //format: main note + '.' or flat/sharp + ' ' + numOfBeats([0-9] or H) + \n + \0
    while(fgets(note, 6, fp) != NULL) {
        f=-1.0;
        for(int i=0;i<sizeof(note);i++)
        {
            note[i]=toupper(note[i]);
        }
        
        float numOfBeats;
        if (note[3] - '0' < 10 && note[3] - '0' > 0) {
            numOfBeats = note[3] - '0';
        } else if (note[3] == 'H') {
            numOfBeats = 0.5;
        } else if (note[3] == '.') {
            numOfBeats = 1.5;
        } else {
            printf("Invalid input format!\n\n");
            return EXIT_FAILURE;
        }
        
        switch(note[0])
        {
            case 'P':
                f=0;
                break;
            case 'C':
                if ( note[1] == 'S') {
                    f = CSHARP4;
                    printf("Playing C sharp...");
                } else {
                    f = C4;
                    printf("Playing C...");
                }
                break;
            case 'D':
                if ( note[1] == 'F') {
                    f = DFLAT4;
                    printf("Playing F flat...");
                } else if (note [1] == 'S') {
                    f = DSHARP4;
                    printf("Playing D sharp...");
                } else {
                    f = D4;
                    printf("Playing D...");
                }
                break;
            case 'E':
                if ( note[1] == 'F') {
                    f = EFLAT4;
                    printf("Playing E flat...");
                } else {
                    f = E4;
                    printf("Playing E...");
                }
                break;
            case 'F':
                if ( note[1] == 'S') {
                    f = FSHARP4;
                    printf("Playing F sharp...");
                } else {
                    f = F4;
                    printf("Playing F...");
                }
                break;
            case 'G':
                if ( note[1] == 'F') {
                    f = GFLAT4;
                    printf("Playing G flat...");
                } else if (note [1] == 'S') {
                    f = GSHARP4;
                    printf("Playing G sharp...");
                } else {
                    f = G4;
                    printf("Playing G...");
                }
                break;
            case 'A':
                if ( note[1] == 'F') {
                    f = AFLAT4;
                    printf("Playing A flat...");
                } else if (note [1] == 'S') {
                    f = ASHARP4;
                    printf("Playing A sharp...");
                } else {
                    f = A4;
                    printf("Playing A...");
                }
                break;
            case 'B':
                if ( note[1] == 'F') {
                    f = BFLAT4;
                    printf("Playing B flat...");
                } else {
                    f = B4;
                    printf("Playing B...");
                }
                break;
            default:
                printf("Not a note.");
        }
        
        printf("\n");
        
        if (f >= 0)
        {
            //open output stream (using default output device) with 0 input channels, 2 output channels (stereo) and 256 frames per buffer
            error = Pa_OpenDefaultStream(&stream, 0, 2, paFloat32, SAMPLE_RATE, 256, streamCallback, &data);
            if(error != paNoError)
            {
                printf("PortAudio error: %s\n", Pa_GetErrorText(error));
                return EXIT_FAILURE;
            }
            
            //start the stream
            error = Pa_StartStream(stream);
            if(error != paNoError)
            {
                printf("PortAudio error: %s\n", Pa_GetErrorText(error));
                return EXIT_FAILURE;
            }
            
            //sleep while stream is running (for 5 seconds)
            Pa_Sleep(beatDuration*numOfBeats);
            
            //close stream after 5 seconds of streaming
            error = Pa_CloseStream(stream);
            if(error != paNoError)
            {
                printf("PortAudio error: %s\n", Pa_GetErrorText(error));
                return EXIT_FAILURE;
            }
        }
    }
    if (feof(fp)) {
        printf("\nEnd of file reached\n");
    }
    fclose(fp);
    return EXIT_SUCCESS;
}