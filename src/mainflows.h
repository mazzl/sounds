/** Project for "Laboratorio di Ingegneria Informatica"
 * @copyright Copyright © 2016 Luca Mazzon. All rights reserved.
 *
 * @file mainflows.h
 *
 * @brief This file contains the declarations of the two funtions called by the main
 * @author Luca Mazzon
 * @since 0.3
 */

#ifndef mainflows_h
#define mainflows_h

extern int playFromStdIn(PaStream *stream, double beatDuration);
extern int playFromFile(PaStream *stream, char *filename, double beatDuration);

#endif /* mainflows_h */
