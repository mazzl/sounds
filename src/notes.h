/** Project for "Laboratorio di Ingegneria Informatica"
  * @copyright Copyright © 2016 Luca Mazzon. All rights reserved.
  *
  * @file notes.h
  * Sounds
  *
  * @author Luca Mazzon
  * @copyright Copyright © 2016 Luca Mazzon. All rights reserved.
  *
  * @since 0.2
  *
  * @brief Contains the definitions of the MACROS which contain the frequency associated with the notes.
  * @details The frequency values used are taken from the fourth octave, the one that contains the middle C.
  */

#ifndef notes_h
#define notes_h

#undef C4
#undef CSHARP4
#undef DFLAT4
#undef D4
#undef DSHARP4
#undef EFLAT4
#undef E4
#undef F4
#undef FSHARP4
#undef GFLAT4
#undef G4
#undef GSHARP4
#undef AFLAT4
#undef A4
#undef ASHARP4
#undef BFLAT4
#undef B4

#define C4 (261.63)
#define CSHARP4 (277.18)
#define DFLAT4 (277.18)
#define D4 (293.66)
#define DSHARP4 (311.13)
#define EFLAT4 (311.13)
#define E4 (329.63)
#define F4 (349.63)
#define FSHARP4 (369.99)
#define GFLAT4 (369.99)
#define G4 (392.00)
#define GSHARP4 (415.30)
#define AFLAT4 (415.30)
#define A4 (440.00)
#define ASHARP4 (466.16)
#define BFLAT4 (466.16)
#define B4 (493.88)

#endif /* notes_h */
